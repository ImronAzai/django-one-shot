from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.form import TodoListForm, TodoItemForm


def view_todolist(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todolists/list.html", context)


def view_task(request, id):
    tasks = TodoList.objects.get(id=id)
    context = {"tasks": tasks}
    return render(request, "todolists/detail.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("view_task", id=list.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todolists/create.html", context)


def edit_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("view_task", id=list.id)
    else:
        form = TodoListForm(instance=list)
    context = {"form": form}

    return render(request, "todolists/edit.html", context)


def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("view_todolist")
    return render(request, "todolists/delete.html")


def create_task(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("view_task", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todolists/newtask.html", context)


def update_item(request, id):
    list = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("view_task", id=list.id)
    else:
        form = TodoItemForm(instance=list)
    context = {"form": form}
    return render(request, "todolists/update.html", context)
